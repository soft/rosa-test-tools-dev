#!/bin/bash
#Сопоставление пользователя с устройством.
#Инициализация токена, создание сертификата, настройка pam, проверка входа.

error() { echo -e "ERROR: $1"; exit 1; }

SCNOTFOUND="No smart card readers found"
if [[ "`/usr/bin/opensc-tool --list-readers`" == "$SCNOTFOUND" ]]
  then error $SCNOTFOUND
fi

/usr/bin/pkcs15-init --erase-card 2> /dev/null;
if [ $? != 0 ]; then error "Erase card"; fi;

/usr/bin/pkcs15-init --create-pkcs15 --so-pin "12345678" --so-puk "" 2> /dev/null;
if [ $? != 0 ]; then error "Create-pkcs15"; fi;

/usr/bin/pkcs15-init --store-pin --label "User PIN" --auth-id 02 --pin "12345678" --puk "" --so-pin "12345678" --finalize 2> /dev/null;
if [ $? != 0 ]; then error "Store pin"; fi;

/usr/bin/pkcs15-init -G rsa/1024 --auth-id 02 --label "My Private Key" --public-key-label "My Public Key" --pin "12345678" 2> /dev/null;
if [ $? != 0 ]; then error "Key generation"; fi;

ID=`/usr/bin/pkcs15-tool --list-keys | grep -E "^[[:space:]]ID" | awk -F: '{ print $2 }' | sed 's/\ //g' 2> /dev/null`
#echo "Token ID:"$ID\n

/usr/bin/openssl > /dev/null 2>&1 << __SSL__
engine dynamic -pre SO_PATH:/usr/lib64/openssl/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so
req -passin pass:12345678 -engine pkcs11 -new -key slot_1-id_$ID -keyform engine -x509 -out cert.pem -subj "/C=RU/ST=Moscow/L=Moscow/O=NTCIT/OU=IT/CN=test/emailAddress=test@example.ru"
__SSL__
if [ $? != 0 ]; then error "Openssl"; fi;

/usr/bin/pkcs15-init --store-certificate cert.pem --auth-id 02 --id 02 --format pem --pin "12345678" 2> /dev/null;
if [ $? != 0 ]; then error "Store certificate"; fi;

mv cert.pem /etc/pam_pkcs11/cacerts
cd /etc/pam_pkcs11/cacerts
/usr/bin/pkcs11_make_hash_link

useradd test > /dev/null 2>&1
echo "test" | passwd test --stdin > /dev/null
cp -f /etc/pam.d/system-auth /etc/pam.d/system-auth.bak
AUTHSTR="auth  sufficient pam_pkcs11.so config_file=/etc/pam_pkcs11/pam_pkcs11.conf"
sed -i -e "/auth.* required.* pam_env.so/i$AUTHSTR" /etc/pam.d/system-auth

if [ "`echo "12345678" | su -c whoami test`" != "test" ]; then error "Test login"; fi;

cp -f /etc/pam.d/system-auth.bak /etc/pam.d/system-auth

exit 0