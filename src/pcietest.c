// usage insmod pcie_test.ko vendor_id="0x10de" device_id="0x1245"

#include <linux/init.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/unistd.h>

MODULE_LICENSE("GPL v2");

#define MODULE_NAME "pcie_test: "

static char* vendor_id = NULL;
module_param(vendor_id, charp, S_IRUSR | S_IWUSR | S_IXUSR);
MODULE_PARM_DESC(vendor_id, "PCI vendor id to match");

static char* device_id = NULL;
module_param(device_id, charp, S_IRUSR | S_IWUSR | S_IXUSR);
MODULE_PARM_DESC(device_id, "PCI device id to match");


#define AER_CAP_ID_VALUE	0x14011

//returns Zero if device doesn't have PCI-E capabilities
static int get_pci_pcie_cap2(struct pci_dev *dev)
{
	u16 flags;
	int pos;
	pos = pci_find_capability(dev, PCI_CAP_ID_EXP);
	return pos;
}

static int __init pcie_test_start(void)
{
	printk(MODULE_NAME "running test.\n");

	if ( !vendor_id && !device_id )
	{
		printk(MODULE_NAME "unknown device %s and vendor IDs %s.\n", vendor_id, device_id);
		return -1;
	}
	unsigned int vid, did;

	if ( !sscanf(vendor_id, "%x", &vid) )
		return -1;
	if ( !sscanf(device_id, "%x", &did) )
		return -1;

    struct pci_dev *pDev = NULL;
    pDev = pci_get_device(vid, did, NULL);

    if(!pDev)
    {
    	printk(MODULE_NAME "device not found\n");
    	return -1;
    }
    else
    {
    	printk(MODULE_NAME "device found\n");
    	int cap = 0;
    	if ( ! (cap = get_pci_pcie_cap2(pDev))  )
    	{
    		printk(MODULE_NAME "device doesn't have PCI-E capabilities.");
    		return -1;
    	}
    	else
    	{
    		printk(MODULE_NAME "device has PCI-E capabilities. Capabilities flag: %d\n", cap);
    	}
    }
    return 0;
}

static void __exit pcie_test_exit(void)
{
	printk(MODULE_NAME "removed from kernel\n");
}

module_init(pcie_test_start);
module_exit(pcie_test_exit);
