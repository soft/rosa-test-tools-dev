#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

static XKeyEvent createKeyEvent(Display *display, Window win,
                           Window winRoot, bool press,
                           int keycode, int modifiers) {
    XKeyEvent event;

    event.display     = display;
    event.window      = win;
    event.root        = winRoot;
    event.subwindow   = None;
    event.time        = CurrentTime;
    event.x           = 1;
    event.y           = 1;
    event.x_root      = 1;
    event.y_root      = 1;
    event.same_screen = True;
    event.keycode     = XKeysymToKeycode(display, keycode);
    event.state       = modifiers;
    event.type = press ? KeyPress : KeyRelease;
    return event;
}

static Status sendPressReleaseEvents(Display *display, Window win, Window winRoot,
                                    int keycode, int modifiers) {
    XKeyEvent event;
    int status;

    event = createKeyEvent(display, win, winRoot, true, keycode, modifiers);
    status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    if (!status) return status;
    event = createKeyEvent(display, win, winRoot, false, keycode, modifiers);
    status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    if (!status) return status;
    XFlush(display);
    usleep(500 * 1000);
    return status;
}

static void sendCloseEvent(Display *display, Window window, Window rootWindow) {
    XEvent ev;

    memset(&ev, 0, sizeof(ev));
    Atom closeWindowAtom = XInternAtom(display, "_NET_CLOSE_WINDOW", False);
    ev.xclient.type = ClientMessage;
    ev.xclient.window = window;
    ev.xclient.message_type = closeWindowAtom;
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = CurrentTime;
    ev.xclient.data.l[1] = rootWindow;
    XSendEvent(display, rootWindow, False, SubstructureRedirectMask, &ev);
    XFlush(display);
    usleep(500 * 1000);
}

static bool windowHasName(Display *display, Window win, const char *name) {
    XTextProperty wmName;

    int status = XGetWMName(display, win, &wmName);
    bool result = false;
    if (status) {
        if (wmName.value) {
            if (wmName.nitems) {
                char **list;
                int i;
                status = Xutf8TextPropertyToTextList(display, &wmName, &list, &i);
                if (status >= Success && *list) {
                    result = (strcmp(*list, name) == 0);
                    XFreeStringList(list);
                }
            }
            XFree(wmName.value);
        }
    } else result = (name == NULL);
    return result;
}

static Window findWindow(Display *display, Window currentWindow, const char *targetWindowName) {
    Window parent;
    Window *children;
    unsigned int nNumChildren;

    if (windowHasName(display, currentWindow, targetWindowName)) {
        return currentWindow;
    }

    int status = XQueryTree(display, currentWindow, &currentWindow, &parent, &children, &nNumChildren);
    if (!status || nNumChildren == 0) {
        return None;
    }

    int i;
    Window result = 0;
    for (i = 0; i < nNumChildren; i++) {
        result = findWindow(display, children[i], targetWindowName);
        if (result != None) break;
    }
    XFree((char*) children);
    return result;
}

static int error_handler(Display *display, XErrorEvent *error) {
    return 0;
}

int main(int argc, char **argv) {
    Display *display = XOpenDisplay(0);
    bool done = false;
    if (argc != 2) {
        fprintf(stderr, "Internal Error! Invalid parameters.\n");
        return 1;
    }
    if (display) {
        XSetErrorHandler(error_handler);
        Window rootWindow = XDefaultRootWindow(display);
        Window window = None;
        int t;
        for (t = 0; (t < 100) && (window == None); t++) {
            window = findWindow(display, rootWindow, argv[1]);
            usleep(100 * 1000);
        }
        if (window != None) {
            printf("Target window found. Sending a key sequence...\n");
            XSetInputFocus(display, window,  RevertToParent, CurrentTime);
            //Ctrl+F - Flip image
            sendPressReleaseEvents(display, window, rootWindow, XK_Right, ControlMask + ShiftMask);
            //Ctrl+I - Inverse colors
            sendPressReleaseEvents(display, window, rootWindow, XK_I, ControlMask);
            if (strstr(argv[1], ".gif ") == NULL) {
                //Ctrl+S - Save (KolourPaint can't save GIF format)
                sendPressReleaseEvents(display, window, rootWindow, XK_S, ControlMask);
            } else {
                //Ctrl+Z - Undo (to exit without warning)
                sendPressReleaseEvents(display, window, rootWindow, XK_Z, ControlMask);
                sendPressReleaseEvents(display, window, rootWindow, XK_Z, ControlMask);
            }
            if (strstr(argv[1], ".jpeg ") != NULL) {
                Window cwindow = None;
                usleep(1500 * 1000);
                cwindow = findWindow(display, rootWindow, "Сохранение изображения как — KolourPaint");
                if (cwindow == None) cwindow = findWindow(display, rootWindow, "Save Image As — KolourPaint");
                if (cwindow != None) {
                    // Confirm jpeg format save
                    sendPressReleaseEvents(display, cwindow, rootWindow, XK_KP_Enter, 0);
                    usleep(1000 * 1000);
                    cwindow = findWindow(display, rootWindow, "Формат с потерей качества — KolourPaint");
                    if (cwindow == None) cwindow = findWindow(display, rootWindow, "Lossy File Format — KolourPaint");
                    if (cwindow != None) {
                        // Confirm lossy file format
                        sendPressReleaseEvents(display, cwindow, rootWindow, XK_KP_Enter, 0);
                    }
                }
            }
            sendCloseEvent(display, window, rootWindow);
            done = true;
        } else {
            fprintf(stderr, "Couldn't find a window with the caption '%s'.\n", argv[1]);
        }
        XCloseDisplay(display);
    } else {
        fprintf(stderr, "Couldn't open X display.\n");
    }
    return done ? 0 : 1;
}
