#!/usr/bin/env python3
# 2019@kernelplv : python3.5 : memory shredder check

import sys
import random
import os
from subprocess import STDOUT, PIPE, run

OK = True
BAD = False

rnd_data = ['RoSa', 'Cobalt', 'Nickel', 'password', 'cat in the hat',
            'mr.Smith', '$828.099', 'Rosa @kernelplv', 'C++ better']

# Commands
cmd_sync = ['sudo sync; echo 3 > /proc/sys/vm/drop_caches', '']
cmd_success = ["echo 'shred_test: test passed' > /dev/kmsg", '']

# ~Commands

# function execute command and wait for result.
# args: 
#      command - shell(terminal) command
#      yes     - if not null, send it as answer after execute command 
#      text    - if Text = false, send command as byte-array not text
def call(command: str, yes: str = '', text=True, print_err=True):
    answer = ''

    if yes:
        answer = 'echo ' + yes + ' | '

    result = run(answer + command, shell=True, timeout=25, universal_newlines=text, stdout=PIPE, stderr=STDOUT)
    output = result.stdout

    if result.returncode == 0:
        return output if output \
            else 1
    else:
        if ( print_err ):
            print('ERROR: ' + str(output))
        return BAD
    pass


# DEPEND packages: gostsum
def get_hash(file_path):
    cmd = ['gost12sum {}'.format(file_path), '']
    result = call(*cmd)
    return result.split(' ')[0] if result else BAD


# return 0 if it has error
def create_fs():
    cmd_pull = [
        # create virtual hard disk
        ['dd if=/dev/zero of=vhd_test.img bs=1M count=1', ''],

        # make file system ext4
        ['mkfs -t ext4 vhd_test.img', 'y'],

        # mount
        ['mkdir /mnt/vhd/', ''],
        ['mount -t auto -o loop vhd_test.img /mnt/vhd/', ''],
        ['mount -o remount,rw /mnt/vhd/', '']
    ]

    for cm in cmd_pull:
        if not call(*cm):
            return BAD

    print('OK')
    return OK


def delete_fs(check=False):
    # delete virtual hard disk & filesystem
    cmd_pull = ['umount /mnt/vhd/',
                'rm -rf /mnt/vhd/',
                'rm vhd_test.img'
                ]

    for cm in cmd_pull:
        if not call(cm, print_err= not check):
            if not check:
                return BAD

    return OK


# DEPEND packages: secure-delete
def check_file(tst_file='/mnt/vhd/test.bin', tst_util=['srm -z'], file_size=249):
    cmd_get_file_offset = ['sudo filefrag -k -v /mnt/vhd/test.bin '
                           '| tail -3 | head -n2 | cut -d: -f3 | tail -1 | cut -d. -f3', '']

    cmd_shred = ['sudo {} {} '.format(tst_util, tst_file), '']

    call(*cmd_sync)  # sync or wait for 15-30 sec
    offset = int(call(*cmd_get_file_offset))

    print('\nFilesystem offset: {}'.format(offset))

    cmd_read_file_by_offset = ['sudo dd if=/dev/loop0 bs=1024 count=1 '
                               'skip={} status=none | dd bs={} count=1 status=none'.format(offset, file_size), '']

    file = call(*cmd_read_file_by_offset, text=False)
    call(*cmd_sync)

    print('\n[ Before shred ({}): begin ]\n'.format(tst_util))
    print('\t{}'.format(file))
    print('\n[ Before shred ({}): end ]\n'.format(tst_util))

    call(*cmd_shred)
    call(*cmd_sync)

    file = call(*cmd_read_file_by_offset, text=False)
    call(*cmd_sync)
    

    print('\n[ After shred ({}): begin ]'.format(tst_util))
    print('\n\t{}'.format(file))
    print('\n[ After shred ({}): end ]\n'.format(tst_util))

    print('Check for restore.. ', end='')
    for byte in file:
        if byte != 0:
            print('ERROR: the file contains the remains of the previous.')
            return BAD

    print('OK')

    return OK


def create_file(tst_file='/mnt/vhd/test.bin'):
    random.seed()

    print('\n[ Writing file test.bin: begin ]\n')
    line = ''
    with open(tst_file, 'w') as F:
        for _i in range(5):
            line = ''
            for _j in range(5):
                line += random.choice(rnd_data) + ' '
            F.write(line + '\n')
            print('\t{}'.format(line))

    print('\n[ Writing file test.bin: end. Size of "{file}": {fsize} ]\n'.format(file=tst_file, fsize=os.path.getsize(tst_file)))

    # print('Gost12 Hash: {hash}'.format(hash=get_hash(tst_file)))

    print('\n[ Reading file test.bin: begin ]\n')
    with open(tst_file, 'r') as F:
        for line in F:
            print('\t' + line, end='')
    print('\n[ Reading file test.bin: end ]\n')

    info = os.stat(tst_file)
    print('File info: {}'.format(info))
    return info.st_size


def main():
    print('\nCreating file system.. ', end="")

    delete_fs(check=True)
    if create_fs():
        if check_file(file_size=create_file(), tst_util='rm'):
            print('rm - ok. It is WRONG')
        elif check_file(file_size=create_file(), tst_util='srm -z'):
            print('srm - ok. It is GOOD')
            call(*cmd_success)
        print('Deleting virtual hard drive.. ', end="")
        if delete_fs():
            print('OK')
        else:
            print('Could not unmount, a reboot may be required.')
    else:
        print('BAD - need restart PC')

    call(*cmd_sync), print('')
    return 0


if __name__ == "__main__":
    sys.exit(main())
