/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  fb_test.cpp                                            :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: kernelplv <kernelplv@gmail.com>                  +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2020/01/28 13:59:20 by kernelplv          #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2020/02/03 12:31:49 by kernelplv         ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

// MIT license


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/kd.h>
#include <thread> 
#include <chrono>
#include <iostream>
#include <unistd.h>
#include <linux/vt.h>
#include "tools.h"

using namespace std;
using namespace chrono_literals;

const char fbdev[]  = "/dev/fb0";
const char ttydev[] = "/dev/tty0";

struct Screen {
    char         *buffer;
    size_t        size;
    size_t        bytes_per_pixel, bytes_per_line;
    size_t        width, height;
    uint_fast16_t red, green, blue;
};

struct fb_var_screeninfo  info;
struct fb_fix_screeninfo _info;


int main( int argc, char **argv) {

    ios::sync_with_stdio(false);

    int ttyfd = open( ttydev, O_RDWR );

    if ( ttyfd < 0 )
        ERR("Cant open tty!");

    // change tty
    if ( ioctl( ttyfd, VT_ACTIVATE,   2) == -1 or
         ioctl( ttyfd, VT_WAITACTIVE, 2) == -1 ) {
        
        ERR("Cant change tty!");
    }
    
    if ( ioctl( ttyfd, KDSETMODE, KD_GRAPHICS) == -1)
        ERR("Cannot set tty into graphics mode!");

    int fbfd = open( fbdev, O_RDWR );

    if ( fbfd < 0 )
        ERR("Cannot open framebuffer");
    
    if ( ioctl( fbfd, FBIOGET_FSCREENINFO, &_info) == -1 )
        ERR("Cannot open fixed screen info!");
    if ( ioctl( fbfd, FBIOGET_VSCREENINFO, &info)  == -1 )
        ERR("Cannot open variable screen info!");

    Screen scr = {
        .size            = info.yres * _info.line_length,
        .bytes_per_pixel = info.bits_per_pixel / 8, 
        .bytes_per_line  = _info.line_length,
        .red             = info.red.offset / 8,
        .green           = info.green.offset / 8,
        .blue            = info.blue.offset / 8,
        .width           = info.xres,
        .height          = info.yres
    };

    scr.buffer = reinterpret_cast<char *> ( mmap( 0, scr.size, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0 ) );

    if ( scr.buffer == MAP_FAILED )
        ERR("Cannot map framebuffer!");
    
    
    chronobot<chrono::seconds> counter;
    
    uint pix_offset = 0;
    for ( uint t = 0; t < 255; t++ )
        for ( uint y = 0; y < info.yres; y++ )
            for ( uint x = 0; x < info.xres; x++ ) {
                pix_offset = x * scr.bytes_per_pixel + y * scr.bytes_per_line;
                scr.buffer[ pix_offset + scr.red ]   = x * 255 / scr.width;
                scr.buffer[ pix_offset + scr.green ] = y * 255 / scr.height;
                scr.buffer[ pix_offset + scr.blue ]  = t;
            }

    if ( munmap (scr.buffer, scr.size) == -1 ) {

        ERR("Error while filling buffer!");
        ioctl( ttyfd, VT_ACTIVATE, 1);
        ioctl( ttyfd, VT_WAITACTIVE, 1);
    }


    if ( ioctl( ttyfd, KDSETMODE, KD_TEXT ) == -1 )
        ERR("Cannot set tty into text mode!");
    
    if ( ioctl( ttyfd, VT_ACTIVATE, 1)   == -1 or
         ioctl( ttyfd, VT_WAITACTIVE, 1) == -1 ) {

        ERR("Cannot return to tty1!");
    }

    close( fbfd );
    close( ttyfd );

    cout << "FPS: " << 255.0 / counter.check() << endl;
    
    exit(EXIT_SUCCESS);
}



