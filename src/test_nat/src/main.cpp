// @kernelplv <kernelplv@gmail.com>
// www.rosalinux.ru

#include <iostream>
#include <memory>
#include <thread>
#include <future>
#include <cstdlib>
#include "server.h"
#include "client.h"
#include <sched.h>
#include <stdio.h>
#include <chrono>
#include "boost/asio/deadline_timer_service.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"

using namespace std;
using namespace boost::asio;
using socket_ptr = shared_ptr<ip::tcp::socket>;
namespace po = boost::program_options;

void AppInit(int argc, char *argv[])
{
    po::options_description desc("\"NAT Test (test_nat) \" 0.1 ROSA 2019@kernelplv \n No need options, just start!");
    desc.add_options()
        ("help", "Produce help message")
    ;
    po::variables_map vars;
    po::store(po::parse_command_line(argc, argv, desc), vars);
    po::notify(vars);

    // Args processing
    if ( vars.count("help") )
    {
        cout << desc << "\n";
    }

}

int main (int argc, char *argv[]) {
    
    AppInit(argc, argv);

    ao::io_service io_service_server, io_service_client;

    atomic<int> timeout(0);
    ao::deadline_timer server_timer(io_service_server, boost::posix_time::seconds(10));
    ao::deadline_timer client_timer(io_service_client, boost::posix_time::seconds(10));

    server_timer.async_wait(
        [&io_service_server, &timeout](const boost::system::error_code &e){
            cout << "\nServer waiting timeout!" << endl;
            timeout = 1;
            io_service_server.stop();
        }
    );

    client_timer.async_wait(
        [&io_service_client, &timeout](const boost::system::error_code &e){
            cout << "\nClient waiting timeout!" << endl;
            timeout = 1;
            io_service_client.stop();
        }
    );

    tcp::resolver resolver(io_service_client);

    int fd = open("/run/netns/V2", O_RDONLY);
    setns(fd, CLONE_NEWNET);

    //system("ifconfig");
    server s1( io_service_server, 1314); //Network2 with 192.168.2.2

    fd = open("/run/netns/V1", O_RDONLY);
    setns(fd, CLONE_NEWNET);

    //system("ifconfig");
    client c1( io_service_client,
               resolver.resolve( {"192.168.2.2", "1314"} ) ); //Network1 with 192.168.1.2
    
    promise<size_t> server_off;
    auto server_status = server_off.get_future();

    std::thread (
    [&io_service_server, &server_off, &client_timer]()
    {
        size_t err = 0;

        try
        {
           cout << "Server thread started." << endl;
           io_service_server.run();
           cout << "Server thread stoped." << endl;
           client_timer.cancel();
        }
        catch (size_t e)
        {
            err = e;
            cout << "Message has been corrupted. bad network." << endl;
            io_service_server.stop();
        }

        server_off.set_value_at_thread_exit(err);

    }).detach();

    cout << "Client thread started." << endl;
    io_service_client.run();
    cout << "Client thread stoped." << endl;

    server_status.wait();

    return server_status.get() + timeout;
}
