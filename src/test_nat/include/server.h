#pragma once

#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <memory>
#include "session.h"

namespace ao = boost::asio;
using ao::ip::tcp;

class server
{
    tcp::acceptor acceptor_;
    tcp::socket socket_;

public:
    server(ao::io_service &io_service, short port, std::string address = "")
        : acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
          socket_(io_service)
    {
        if  ( !address.empty() )
            acceptor_ = tcp::acceptor(io_service, tcp::endpoint( ao::ip::address_v4::from_string(address), port ));

        do_accept();
    }

private:
    void do_accept()
    {
        acceptor_.async_accept(socket_,
        [this] ( const boost::system::error_code &e )
        {
            if ( !e )
            {
                std::make_shared<session>( std::move(socket_) ) -> start();
                std::cout << "Server: session started." << std::endl;
            }
            else 
                std::cout << "ERROR: " << e << std::endl;
                
                do_accept();

        });
    }

};
