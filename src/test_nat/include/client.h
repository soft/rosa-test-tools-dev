#pragma once

#include <boost/asio.hpp>
#include <random>
#include <iostream>
#include <algorithm>
#include <cassert>

using boost::asio::ip::tcp;

namespace ao = boost::asio;


class client
{
    static const int MSG_SIZE = 64;

    ao::io_service &io_service_;
    tcp::socket socket_;
    std::array<char, MSG_SIZE> message;
    std::array<char, MSG_SIZE> buff;


public:
    client( ao::io_service &io_service, tcp::resolver::iterator endpoint_it )
        : io_service_(io_service),
          socket_(io_service)
    {
        for (int i = MSG_SIZE -1; i >= 0; i--)
            message[i] = '0' + rnd_digit();
        std::cout << "Client: message generated: "
                  << std::string(message.begin(), message.end())
                  << std::endl;
        do_connect(endpoint_it);
    }

private:
    static char rnd_digit()
    {
        std::random_device rd;
        std::mt19937_64 gen(rd());
        std::uniform_int_distribution<char> dis(0,9);

        return dis(gen);
    }

    void do_connect( tcp::resolver::iterator endpoint_it)
    {
        ao::async_connect(socket_, endpoint_it,
        [this] (const boost::system::error_code &e,
                          tcp::resolver::iterator endpoint_it )
        {
            if ( !e )
            {
                //io_service_.post( [this] () { do_write(); } );
                do_write();
            }
            else 
                std::cout << "ERROR: " << e << std::endl;
        });
    }

    void do_write(std::string msg = "")
    {
        if ( !msg.empty() )
            std::copy(msg.begin(), msg.end(), message.data());

        ao::async_write(socket_, ao::buffer(message),
        [this, msg] (const boost::system::error_code &e, std::size_t)
        {
            std::cout << "Client: writing.." << std::endl;

            if ( !e )
            {
                if ( msg.empty() ) 
                    do_read();
            }
            else {
                std::cout << "ERROR: " << e << std::endl;
                socket_.close();
            }
        });
    }

    void do_read()
    {
        ao::async_read( socket_, ao::buffer(buff),
        [this] (const boost::system::error_code &e, std::size_t)
        {
            std::cout << "Client: recieved message: "
                      << std::string(buff.begin(),buff.end())
                      << std::endl;

            if ( not e && std::equal( buff.begin(), buff.end(),
                                      message.begin(), message.end()) )
            {
               std::cout << "Client: read done! RIGHT answer recieved." << std::endl;
               do_write("stop-good");
            }
            else
            {
                std::cout << "Client: read done! WRONG answer recieved." << std::endl;
                do_write("stop-wrong");
            }
            std::cout << "Client: closing socket.." << std::endl;
            socket_.close();
            io_service_.stop();
        });
    }

};
