 
// ┌──────────────────────────────────────────────────┐        
// │ kernelplv@gmail.com || m.mosolov@rosalinux.ru    │
// │ www.rosalinux.ru                                 │
// │ xtool - Test tool for applications with X11 GUI  │
// ╘══════════════════════════════════════════════════╛

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/keysymdef.h>
#include <chrono>
#include <thread>
#include <string.h>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <set>

using namespace std;
using namespace chrono_literals;

class Timer
{
    chrono::time_point<chrono::steady_clock> start    = chrono::steady_clock::now();
    chrono::duration<long, milli>            duration = chrono::milliseconds(1000);
    chrono::milliseconds                     interval = chrono::milliseconds(100);

    public:

    Timer(unsigned int duration, unsigned int check_interval) 
        : duration( chrono::milliseconds( duration ) ), 
          interval( chrono::milliseconds( check_interval ) )
    {}
    
    bool tick() {

        this_thread::sleep_for( interval );

        auto stop = chrono::steady_clock::now();
        if ( chrono::duration_cast<chrono::milliseconds>(stop - start).count() > duration.count() )
            return false;
        else
            return true;
    }
};

unsigned long windowGetPid(Display *display, Window win)
{
  Atom atom_pid = XInternAtom( display, "_NET_WM_PID", true );

  if ( atom_pid == None ) {
    cerr << "Xtool: atom '_NET_WM_PID not found!" << endl;
    return 0;
  }
  
  Atom          ret_type;
  int           ret_format;
  unsigned long ret_count_items;
  unsigned long ret_count_bytes;
  unsigned char *property = 0;
  unsigned long pid = 0;

  int err = XGetWindowProperty( display, win, atom_pid, 0, 1, false, XA_CARDINAL, 
                                &ret_type, &ret_format, &ret_count_items, &ret_count_bytes, &property );

  if (err != 0)
    return 0;
  else
  {
    pid = * reinterpret_cast<unsigned long *>( property );

    XFree( property );
    return pid;
  }
  
  return 0;
} 

static void sendCloseEvent(Display *display, Window rootWindow, Window window) 
{
    XEvent ev;

    memset(&ev, 0, sizeof(ev));
    Atom closeWindowAtom = XInternAtom(display, "_NET_CLOSE_WINDOW", False);
    
    ev.xclient.type = ClientMessage;
    ev.xclient.window = window;
    ev.xclient.message_type = closeWindowAtom;
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = CurrentTime;
    ev.xclient.data.l[1] = rootWindow;
    XSendEvent(display, rootWindow, False, SubstructureRedirectMask, &ev);
}

XKeyEvent MakeXKeyEvent( Display * display, Window win, Window rwin, bool press, KeyCode keycode, int modifiers ) 
{
    XKeyEvent event;

    event.display     = display;
    event.window      = win;
    event.root        = rwin;
    event.subwindow   = None;
    event.time        = CurrentTime;
    event.x           = 1;
    event.y           = 1;
    event.x_root      = 1;
    event.y_root      = 1;
    event.same_screen = True;
    event.keycode     = keycode;
    event.state       = modifiers;

    event.type = press ? KeyPress : KeyRelease;
    
    return event;
}

bool sendKey(string key, int modifiers, Display *display, Window win, Window rwin, unsigned int delay_ms = 100) 
{
    
    KeySym  skey = XStringToKeysym(key.c_str());
    if ( skey == NoSymbol ) return false;
    KeyCode ckey = XKeysymToKeycode(display, skey);
    
    XSync(display, False);

    // press
    auto event = MakeXKeyEvent(display, win, rwin, true, ckey, modifiers);
    XSetInputFocus(display, win, RevertToParent, CurrentTime);
    XSendEvent(event.display, event.window, True, KeyPressMask, reinterpret_cast<XEvent*> (&event) );
    //XFlush(display);

    // pause
    //this_thread::sleep_for(chrono::milliseconds(delay_ms));

    // release
    event = MakeXKeyEvent(display, win, rwin, false, ckey, modifiers);
    XSetInputFocus(display, win, RevertToParent, CurrentTime);
    XSendEvent(event.display, event.window, True, KeyPressMask, reinterpret_cast<XEvent*> (&event) );

    //XFlush(display);
    return true;
}

bool windowHasName(Display *display, Window win, const char *name) 
{

    auto _cmp = [](const char *strSrc, const char *strTar) {
        //cout << strSrc << endl;
        string _src(strSrc), _tar(strTar);
        return _src.find(_tar) != string::npos;
    };

    XTextProperty wmName;

    int status = XGetWMName(display, win, &wmName);
    bool result = false;
    if (status) {
        if (wmName.value) {
            if (wmName.nitems) {

                char **list;
                int i;
                status = Xutf8TextPropertyToTextList(display, &wmName, &list, &i);

                if (status >= Success && *list) {

                    result = _cmp(*list, name);
                    XFreeStringList(list);
                }
            }
            XFree(wmName.value);
        }
    } //else result = (name == NULL);
    return result;
}

static Window findWindow( Display *display, Window currentWindow,
                          const char *targetWindowName, bool lastWindow = false, bool reset = false) 
{

    static list<Window> wstack;
    static long         lvl = 0;

    if (reset) {
      wstack.clear();
      lvl = 0;
    }

    Window       parent;
    Window       *children;
    unsigned int countChildren;
    
    lvl++;

    if ( windowHasName(display, currentWindow, targetWindowName) )
      wstack.push_back(currentWindow);

    int status = XQueryTree(display, currentWindow, &currentWindow, &parent, &children, &countChildren);

    if ( !status || countChildren == 0 ) 
      return None;

    Window result = None;
    //need to remove recoursive
    for ( unsigned int i = 0; i < countChildren; i++ ) {
      result = findWindow(display, children[i], targetWindowName, lastWindow);
      lvl--;
    }

    XFree((char*) children);

    if (lvl == 1) {

      if (lastWindow)
        result = wstack.back();
      else
        result = wstack.front();
    }

    return result;
}

static int error_handler(Display *display, XErrorEvent *error) 
{
    cout << error->error_code << endl;
    return 0;
}

map<string, string> appinit(int argc, char **argv, unsigned int *return_timeout) 
{
    auto tokens = [](string str, char delim) {
        stringstream ss(str);
        set<string>  tks;
        string       buff;

        while(getline(ss, buff, delim))
            tks.insert(buff);
            
        return tks;
    };

    map<string, string> args;

    auto arg = string( argv[0] );
    for ( int a = 1 ; a < argc; ) 
    {
        arg = string( argv[a] );

        if ( arg.find("help") != string::npos ) 
        {
            cout << "Command line tool for closing X11 apps OR send them KeyPressRelease event. \n\n"
                 << "\txtool \"window title\"            \t close app(window) with title \"window title\" \n"
                 << "\txtool \"window title\" -k Key     \t send KeyPressed without closing app   \n"
                 << "\txtool \"window title\" -r         \t select deepest window with the same name in XTree \n"
                 << "\txtool \"window title\" -t         \t timeout(ms, uint > 0)                 \n"
                 << "\txtool \"window title\" -k Key -x  \t close window after send key           \n"
                 << "\txtool \"window title\" -p         \t print pid of window without closing app \n"
                 << "\t                                  \t  and error messages (only 0 or pid) \n\n"
                 << "Example:                                                                     \n"
                 << "\txtool \"KWrite\" -k BackSpace                                              \n"
                 << "\txtool \"KWrite\" -r                                                        \n"
                 << "\txtool \"KWrite\" -r -k Control+shift+s                                     \n"
                 << "\txtool \"KWrite\" -t 2000 -k Return                                         \n"
                 << "\txtool \"KWrite\" -k Return -x                                              \n"
                 << "\txtool \"KWrite\" -t 2000 -p                                              \n\n"
                 << "\tp.s.: see Keys in X11/keysymdef.h!                                       \n\n"
                 << "www.RosaLinux.ru [ kernelplv@gmail.com ]" << endl;
            args["error"] = "exit";
            return args;
        }
        if ( arg.find("-r") != string::npos ) 
        {
            args[arg] = "lastWindow mode";
            a++;
            continue;
        } 
        if ( arg[0] == '-' ) {
            args[arg] = a + 1 < argc ? string( argv[a+1] ) : "";
            a += 2;
        } else {
            args[""] = arg;
            a++;
        }
    }
    //cout << args["-k"] << endl;
    if ( args.count("-k") && args["-k"].empty() ) {
        
        cerr << "Xtool: argument -k expected Key!" << endl;
        args["error"] = "1";
    }
    else if ( args.count("-k") ) {
        auto ModNKey = tokens(args["-k"],'+');

        for (string t : ModNKey) {

            if ( t == "Shift" || t == "shift" )
                args["shift"] = "mask used";
            else if ( t == "Control" || t == "control" )
                args["control"] = "mask used";
            else 
                args["key"] = t;
        }
    }
    if ( args.count("-t") ) {

        istringstream input( args["-t"] );

        if ( ( input >> *return_timeout ) .fail() ) {
            cerr << "Xtool: invalid timeout! Set to default: 100ms." << endl;
            *return_timeout = 100;
        }
        if ( args["-t"].empty() ) {

            cerr << "Xtool: wrong timeout-flag value!" << endl;
            args["error"] = "2";
        }
    }
    if ( args[""].empty() ) {

        cerr << "Xtool: window title not specified!" << endl;
        args["error"] = "3";
    }
    if ( args.size() > 6 || args.size() == 0 ) {

        cerr << "Xtool: invalid number of arguments!" << endl;
        args["error"] = "4";
    }

    return args;
}

int main(int argc, char **argv) 
{
  unsigned int timeout = 1000;
  auto         args    = appinit(argc, argv, &timeout);
  
  XInitThreads();

  if ( args.count("error") && args["error"] == "exit" )
    return 0;
  else if ( args.count("error") )
    return 1;

  Display *display = XOpenDisplay("");
  bool     done    = false;
  
  if (display) {           

    Window      rootWindow = XDefaultRootWindow(display);
    Window      window     = 0;
    const char *title      = args[""].c_str();
    bool        recoursive = static_cast<bool>( args.count("-r") );
    bool        key_mode   = static_cast<bool>( args.count("-k") );  
    bool        pid_mode   = static_cast<bool>( args.count("-p") );
    bool        exit_mode  = static_cast<bool>( args.count("-x") );


    XSetErrorHandler(error_handler);
    XSelectInput( display, rootWindow, SubstructureNotifyMask );

    XEvent event;
    Timer  timer( timeout, 100 );
    
    this_thread::yield();
    this_thread::sleep_for(500ms);
    window = findWindow( display, rootWindow, title, recoursive, true );  

    if ( window == None )
      while ( timer.tick() ) 
      {
        if ( XPending(display) ) {

          XNextEvent( display, &event );
          if ( event.type == MapNotify ) 
          {
            XLockDisplay( display );
              do
                window = findWindow( display, event.xmap.event, title, recoursive, true );
              while ( timer.tick() && window == None);
            XUnlockDisplay( display );
            break;
          } 
        }
      }

    if ( window != None )
    {
      XLockDisplay( display );
      //cout << "Xtool: target window found.\n";
      
      if ( key_mode ) 
      {
        //cout << "Xtool: sending Key..." << endl;
        
        int mask = 0;
        if ( args.count("shift") ) 
          mask += ShiftMask;
        if ( args.count("control") )
          mask += ControlMask;

        if ( !sendKey( args["key"], mask, display, window, rootWindow ) ) 
          cerr << "Key " << args["-k"] << " not found!" << endl;
        else 
          done = true;
      }

      if ( pid_mode ) 
      {
        unsigned long pid = windowGetPid( display, window );

        cout << pid << endl;
        done = true;
      }
      
      if ( ( !key_mode && !pid_mode ) || 
           (  key_mode && exit_mode )    )
        {
          //cout << "Xtool: closing window...\n" << endl;
          sendCloseEvent( display, rootWindow, window );
          done = true;
        }

      XUnlockDisplay( display );
    }
    
    if ( pid_mode && !done )
      cout << 0 << endl;
    else if ( !done ) 
      cerr << "Xtool: couldn't find a window with the caption '" << args[""] << "'." << endl;

    XSync( display, true );
    XCloseDisplay( display );

  } else {
    cerr << "Xtool: couldn't open X display!" << endl;
  }

  return done ? 0 : 1;
}
